module Elements exposing (Mdl, about, dialog, drawer, fillText, headerContent, navButton, tooltip, userHeader, viewText)

import Html exposing (..)
import Html.Attributes exposing (class, href, style)
import Lorem
import Material
import Material.Button as Button
import Material.Color as Color
import Material.Dialog as Dialog
import Material.Icon as Icon
import Material.Layout as Layout
import Material.List as Lists
import Material.Options as Options exposing (cs, css)
import Material.Tooltip as Tooltip
import Messages exposing (..)
import Model exposing (..)


type alias Mdl =
    Material.Model


navButton : Model -> Msg -> String -> Color.Color -> Int -> Html Msg
navButton model msg icon color index =
    Button.render Mdl
        [ index ]
        model.mdl
        [ Options.onClick msg

        -- Options.onClick LoginGoogle
        , Button.fab
        , Button.icon
        , Tooltip.attach Mdl [ index ]
        ]
        [ Icon.view icon [ Color.text color ] ]


tooltip : Model -> Int -> String -> Html Msg
tooltip model index tip =
    Tooltip.render Mdl
        [ index ]
        model.mdl
        [ Tooltip.top ]
        [ text tip ]


headerContent : Model -> List (Html Msg)
headerContent model =
    case model.email of
        Just email ->
            [ navButton model About "info" Color.accent 0
            , tooltip model 0 "About this site"
            , navButton model Logout "power_settings_new" Color.primaryContrast 1
            , tooltip model 1 "Logout"
            , let
                screenName =
                    case model.name of
                        Just name ->
                            name ++ " (" ++ email ++ ")"

                        Nothing ->
                            email
              in
                div [] [ text screenName ]
            ]

        Nothing ->
            [ navButton model About "info" Color.accent 10
            , tooltip model 10 "About this site"
            , Button.render Mdl
                [ 11 ]
                model.mdl
                [ Dialog.openOn "click"

                -- Options.onClick LoginGoogle
                , Button.fab
                , Button.icon
                ]
                [ Icon.view "account_circle" [] ]
            ]


userHeader : Model -> List (Html Msg)
userHeader model =
    [ div [ style [ ( "padding", "1em" ), ( "text-align", "right" ) ] ]
        (headerContent model)
    ]


dialog : Model -> Html Msg
dialog model =
    Dialog.view
        []
        [ Dialog.title [ css "font-size" "1.4em" ] [ text "Continue with" ]
        , Dialog.content []
            [ div
                []
                [ Button.render Mdl
                    [ 10 ]
                    model.mdl
                    [ Options.onClick LoginGoogle
                    , cs "waves-effect waves-light btn social google"
                    ]
                    [ i [ class "fa fa-fw fa-google" ] [], text " Google" ]
                , Button.render Mdl
                    [ 11 ]
                    model.mdl
                    [ Options.onClick LoginGoogle
                    , cs "waves-effect waves-light btn social facebook"
                    ]
                    [ i [ class "fa fa-facebook" ] [], text " Facebook" ]
                ]
            ]
        , Dialog.actions []
            [ Button.render Mdl
                [ 0 ]
                model.mdl
                [ Dialog.closeOn "click" ]
                [ text "Close" ]
            ]
        ]


about : Html Msg
about =
    div [] (fillText 5)


fillText : Int -> List (Html Msg)
fillText cnt =
    List.map viewText (Lorem.paragraphs cnt)


viewText : String -> Html Msg
viewText msg =
    div [] [ text msg ]


drawer : Model -> List (Html Msg)
drawer model =
    [ Lists.ul []
        [ Lists.li [ Options.onClick Profile ]
            [ Lists.content []
                [ Lists.icon "perm_identity" []
                , text "Profile"
                ]
            ]
        , Lists.li [ Options.onClick Settings ]
            [ Lists.content []
                [ Lists.icon "settings" []
                , text "Settings"
                ]
            ]
        , Lists.li [ Options.onClick Logout ]
            [ Lists.content []
                [ Lists.icon "power_settings_new" []
                , text "Logout"
                ]
            ]
        ]
    ]
