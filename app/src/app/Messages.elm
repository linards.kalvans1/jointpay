module Messages exposing (..)

import Navigation exposing (Location)
import Material
import Routes exposing (Route)
import Model exposing (Token)


type Msg
    = Mdl (Material.Msg Msg)
    | LoginGoogle
    | OnLocationChange Location
    | UpdateRoute Route
    | About
    | OnLoadToken (Maybe Token)
    | Logout
    | Settings
    | Profile
