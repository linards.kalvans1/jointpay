module Routes exposing (..)

import Navigation exposing (Location)
import UrlParser exposing (..)
import Auth0.UrlParser
    exposing
        ( Auth0CallbackInfo
        , Auth0CallbackError
        , accessTokenUrlParser
        , unauthorizedUrlParser
        )


type Route
    = HomeRoute
    | AboutRoute
    | ErrorRoute
    | AccessTokenRoute Auth0CallbackInfo
    | UnauthorizedRoute Auth0CallbackError
    | SettingsRoute
    | LogoutRoute
    | ProfileRoute


parseLocation : Location -> Route
parseLocation location =
    case parseHash matchers location of
        Just route ->
            route

        Nothing ->
            ErrorRoute


matchers : Parser (Route -> a) a
matchers =
    oneOf
        [ map HomeRoute top
        , map AboutRoute (s "about")
        , map AccessTokenRoute accessTokenUrlParser
        , map UnauthorizedRoute unauthorizedUrlParser
        , map SettingsRoute (s "settings")
        , map LogoutRoute (s "logout")
        , map ProfileRoute (s "profile")
        ]


path : Route -> String
path route =
    case route of
        HomeRoute ->
            "#"

        AboutRoute ->
            "#about"

        SettingsRoute ->
            "#settings"

        LogoutRoute ->
            "#logout"

        ProfileRoute ->
            "#profile"

        ErrorRoute ->
            ""

        _ ->
            ""
