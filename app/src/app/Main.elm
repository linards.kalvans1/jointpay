{- This file re-implements the Elm Counter example (1 counter) with elm-mdl
   buttons. Use this as a starting point for using elm-mdl components in your own
   app.
-}


module Main exposing (..)

import Material
import Messages exposing (..)
import Model exposing (..)
import Navigation
import Pages exposing (Mdl)
import Platform.Cmd exposing (batch)
import Ports exposing (..)
import Routes exposing (..)
import Auth0.UrlParser
    exposing
        ( Auth0CallbackInfo
        , Auth0CallbackError
        )
import Json.Decode as Json exposing (map4, oneOf, field, int, string, Value)
import Jwt exposing (..)
import Debug exposing (log)


updateRoute : Route -> Model -> ( Model, Cmd msg )
updateRoute route model =
    ( { model | route = route }, Navigation.newUrl <| path route )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        -- Boilerplate: Mdl action handler.
        Mdl msg_ ->
            Material.update Mdl msg_ model

        LoginGoogle ->
            ( model, Ports.loginGoogle () )

        OnLocationChange location ->
            ( { model | route = parseLocation location }, Cmd.none )

        UpdateRoute route ->
            updateRoute route model

        About ->
            updateRoute AboutRoute model

        Logout ->
            removeToken model

        Settings ->
            updateRoute SettingsRoute model

        Profile ->
            updateRoute ProfileRoute model

        OnLoadToken token ->
            case token of
                Just token ->
                    ( useToken (Just token) model, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )



-- ( useToken token model, Cmd.none )


type alias Auth0IdData =
    { email : String
    , name : String
    , iat : Int
    , expiry : Int
    }


jwtDecoder : Json.Decoder Auth0IdData
jwtDecoder =
    map4 Auth0IdData
        (field "email" string)
        (field "name" string)
        (field "iat" int)
        (field "exp" int)


createToken : Auth0CallbackInfo -> Maybe Token
createToken auth0Token =
    let
        token =
            Token "" Nothing Nothing
    in
        Just
            { token
                | accessToken = auth0Token.accessToken
                , idToken = auth0Token.idToken
                , expiresIn = auth0Token.expiresIn
            }


useToken : Maybe Token -> Model -> Model
useToken newToken model =
    case newToken of
        Just token ->
            let
                idData =
                    case token.idToken of
                        Just string ->
                            let
                                data =
                                    case decodeToken jwtDecoder string of
                                        Err msg ->
                                            Auth0IdData (toString msg) "" -1 -1

                                        Ok data ->
                                            data
                            in
                                data

                        _ ->
                            Auth0IdData "unknown" "unknown" -1 -1
            in
                { model
                    | token = Just token
                    , email = Just idData.email
                    , name = Just idData.name
                }

        Nothing ->
            model


saveToken : Model -> Cmd msg
saveToken model =
    Ports.put (model.token)


removeToken : Model -> ( Model, Cmd msg )
removeToken model =
    let
        newModel =
            { model | token = Nothing, email = Nothing, name = Nothing }
    in
        case model.token of
            Just token ->
                ( newModel, Ports.put (Nothing) )

            Nothing ->
                ( newModel, Cmd.none )


init : Maybe Token -> Navigation.Location -> ( Model, Cmd Msg )
init token location =
    let
        route =
            parseLocation location

        model =
            case route of
                AccessTokenRoute auth0Token ->
                    useToken (createToken auth0Token) initModel

                _ ->
                    useToken token initModel

        cmd =
            saveToken model
    in
        ( { model | route = route }, cmd )


main : Program (Maybe Token) Model Msg
main =
    Navigation.programWithFlags OnLocationChange
        { init = init
        , view = Pages.view
        , update = update
        , subscriptions = \model -> Ports.get OnLoadToken
        }
