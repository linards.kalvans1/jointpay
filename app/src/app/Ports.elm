port module Ports exposing (..)

import Model exposing (Token)


port loginGoogle : () -> Cmd msg


port put : Maybe Token -> Cmd msg


port get : (Maybe Token -> msg) -> Sub msg
