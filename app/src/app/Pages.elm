module Pages exposing (..)

import Elements exposing (..)
import Html exposing (..)
import Html.Attributes exposing (href, class, style)
import Material
import Material.Button as Button
import Material.Dialog as Dialog
import Material.Options as Options exposing (css, cs)
import Material.Layout as Layout
import Messages exposing (..)
import Model exposing (Model)
import Routes exposing (..)
import Debug


type alias Mdl =
    Material.Model


view : Model -> Html Msg
view model =
    Layout.render Mdl
        model.mdl
        [ Layout.fixedHeader
        ]
        { header =
            userHeader model
        , drawer = drawer model
        , tabs = ( [], [] )
        , main =
            [ div
                [ style [ ( "padding", "2rem" ) ] ]
                [ dialog model
                , case model.route of
                    AboutRoute ->
                        about

                    AccessTokenRoute token ->
                        div [] [ text token.accessToken ]

                    _ ->
                        text "Some other content"
                ]
            ]
        }
