module Model exposing (..)

import Material
import Routes exposing (..)


type alias Token =
    { accessToken : String
    , idToken : Maybe String
    , expiresIn : Maybe Int
    }


type alias Model =
    { mdl :
        Material.Model
    , route : Route
    , token : Maybe Token
    , email : Maybe String
    , name : Maybe String

    -- Boilerplate: model store for any and all Mdl components you use.
    }


initModel : Model
initModel =
    { mdl =
        Material.model
    , route = HomeRoute
    , token = Nothing
    , email = Nothing
    , name = Nothing

    -- Boilerplate: Always use this initial Mdl model store.
    }
