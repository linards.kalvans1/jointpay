import Elm from './app/Main.elm';
import './main.scss';

const mountNode = document.getElementById('app');
const app = Elm.Main.embed(mountNode, JSON.parse(localStorage.getItem("access_token")));

if (module.hot) {
    module.hot.accept();
}

import {WebAuth} from 'auth0-js'

// Change these settings to yours
const webAuth = new WebAuth({
    domain: 'jointpay.eu.auth0.com',
    clientID: 'lZHVHe0zyDkYw2uXl4GS12IgXEuxXOPM',
    // audience: 'JointPay',
    redirectUri: 'http://localhost:3080/',
    scope: 'openid email profile'
});

app.ports.loginGoogle.subscribe( function () {
    webAuth.authorize ({
        connection: 'google-oauth2',
        responseType: 'token id_token'
    })
});

app.ports.put.subscribe(function (item) {
    localStorage.setItem("access_token", JSON.stringify(item));
    app.ports.get.send(JSON.parse(localStorage.getItem("access_token")));
});
